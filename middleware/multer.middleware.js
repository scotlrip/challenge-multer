let multer = require('multer');
let path = require('path');
let storage = multer.diskStorage({
    destination:function(req, file, cb){
        let fullPath = path.resolve(__dirname,"../public/upload");
        cb(null , fullPath);
    },
    filename: function(req, file, cb){
        cb(null,`${file.originalname}_${Date.now()}`);
    }
});

let upload = multer({
    storage: storage,
    fileFilter: function (req, file, cb) {
        var filetypes = /jpeg|jpg/;
        var mimetype = filetypes.test(file.mimetype);
        var extname = filetypes.test(path.extname(file.originalname).toLowerCase());
    
        if (mimetype && extname) {
          return cb(null, true);
        }
        return cb(new Error('Only images are allowed'))
        }
    //     ,
    // limits:{
    //         fileSize: 1024 * 1024
    //     }
    });

exports.upload = upload;