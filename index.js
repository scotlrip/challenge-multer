let express= require('express');
let app = express();
let bodyParser = require('body-parser');
let users =[{username:"a" , pwd :"123", email:"a@gmail.com", gallery:[], avatars:[]}];
let { uploadArr, uploadFields } = require('./helpers/config.upload');
let fs = require('fs');
let path = require('path');

//views config
app.set("view engine", "ejs");
app.set("views", "./views");
//middlewares

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

//routes
app.get('/', (req, res) => {
    res.render('index',{users});
});
//handle user
app.get('/create-user', (req, res) => {
    res.render('createUser');
})
app.post('/add-user', (req, res) => {
    let user = req.body;
    console.log(user);
    let isExist = users.find((item) => item.username === user.username);
    console.log(isExist);
    if(!isExist)
    {
        users.push({username:user.username, pwd :user.pwd, email: user.email, gallery:[], avatars:[]});
        console.log(users);
        res.redirect('/');
    }
    else res.send("User is exist!!");
})
//upload gallery 
app.get('/upload-gallery/:username', (req, res) => {
    let {username} = req.params;
    let infoUser = users.find(user => user.username === username);
    res.render('uploadGallery', {infoUser});
})
app.post('/upload-gallery/:username', uploadArr, (req, res) =>{
    let {username} = req.params;
    let files = req.files;

    users = users.map( user => {
        if(user.username === username)
        {
            files.forEach( file => {
                let time = file.filename.substring(file.filename.lastIndexOf('_')+1);
                user.gallery.push({createAt:time, name:file.filename});
            }) 
            
        }
        return user;
      
    })

    res.redirect('/');
})

//upload gallery and avatar
app.get('/upload-avatar-gallery/:username', (req, res) => {
    let { username } = req.params;
    let infoUser = users.find( user => user.username === username);
    res.render('uploadGalleryAvatar',{infoUser});
})
app.post('/upload-avatar-gallery/:username', uploadFields, (req, res) => {
    let {username} = req.params;
    let {avatar, gallery} = req.files;
    console.log({avatar,gallery});
    users = users.map( user => {
        if(user.username === username)
        {
            gallery.forEach( photo => {
                let time = photo.filename.substring(photo.filename.lastIndexOf('_')+1);
                user.gallery.push({createAt:time, name: photo.filename});
            })
            let time = avatar[0].filename.substring(avatar[0].filename.lastIndexOf("_")+1);
            let checkActived = user.avatars.find( photo => photo.isActive == true);
            if(checkActived)
            user.avatars.push({createAt:time, name:avatar[0].filename, isActive:false});
            else
            user.avatars.push({createAt:time, name:avatar[0].filename, isActive:true});
        }
        return user;
    })
    res.redirect('/');
})
//setAvatar
app.get('/active-avatar/:username/:avatarName', (req, res) => {
    let {username, avatarName} = req.params;
    users= users.map(user=> {
        if(user.username === username)
        {
           
           user.avatars = user.avatars.map(photo => {
                if(photo.name === avatarName)
                {
                    return {
                        ...photo,
                        isActive:true
                         }
                }                  
                return {
                    ...photo,
                    isActive:false
                     }
            });
        }
        return user;
        
    });
    
    
    res.redirect(`/show-photos/${username}`);
});

//show photo
app.get('/show-photos/:username', (req, res) => {
    let { username } = req.params;
    let infoUser = users.find(user => user.username === username);
    res.render('showPhotos', {infoUser});
})

app.get('/remove-gallery/:username/:photoName', (req, res) => {
    let { username, photoName } = req.params;
    let user = users.find( item => item.username === username);
    let indexRemove = user.gallery.findIndex( photo => photo.name === photoName);
    let pathPhotoDelete = path.resolve(__dirname, `./public/upload/${photoName}`);
    user.gallery.splice(indexRemove,1);
    fs.unlinkSync(pathPhotoDelete);
    res.redirect(`/show-photos/${username}`);
})
app.get('/remove-avatar/:username/:photoName', (req, res) => {
    let { username, photoName } = req.params;
    let user = users.find( item => item.username === username);
    let indexRemove = user.avatars.findIndex( photo => photo.name === photoName);
    let pathPhotoDelete = path.resolve(__dirname, `./public/upload/${photoName}`);
    user.avatars.splice(indexRemove,1);
    fs.unlinkSync(pathPhotoDelete);
    res.redirect(`/show-photos/${username}`);
})

let PORT = 3000;
app.listen(PORT, () => {
    console.log("Server start at:", PORT);
})