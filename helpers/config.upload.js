let {upload} = require("../middleware/multer.middleware");

let uploadArr = upload.array('gallery',2);
let uploadFields = upload.fields([{name: "gallery", maxCount: 2}, {name: "avatar", maxCount: 1}]);
module.exports = {uploadArr, uploadFields};